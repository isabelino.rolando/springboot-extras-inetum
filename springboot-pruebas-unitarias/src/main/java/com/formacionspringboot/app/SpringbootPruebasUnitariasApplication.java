package com.formacionspringboot.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootPruebasUnitariasApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootPruebasUnitariasApplication.class, args);
	}

}
