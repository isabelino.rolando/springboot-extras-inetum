package com.formacionspringboot.app;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import com.formacionspringboot.app.dao.ProductoDao;
import com.formacionspringboot.app.entity.Producto;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class ProductoTest {

	@Autowired
	private ProductoDao productoDao;
	
	@Test
	@Rollback(false)
	public void testGuardarProducto() {
		Producto producto = new Producto("TV Samsung HD",500);
		Producto producto2 = new Producto("Smart TV Samsung HD",500);
		productoDao.save(producto);
		Producto productoGuardado = productoDao.save(producto2);
		
		assertNotNull(productoGuardado);
		
	}
	
	@Test
	public void testBuscarProductoPorNombre() {
		String nombre= "TV Samsung HD";
		
		Producto producto = productoDao.findByNombre(nombre);
		
		assertThat(producto.getNombre()).isEqualTo(nombre);
	}
	
	@Test
	public void testBuscarProductoPorNombreNoExistente() {
		String nombre= "Apple Iphone 11";
		
		Producto producto = productoDao.findByNombre(nombre);
		
		assertNull(producto);
	}
	
	
	@Test
	@Rollback(false)
	public void testActualizarProducto() {
		String nombre= "TV Sony HD";
		
		Producto producto = new Producto(nombre,700);
		producto.setId((long) 1);
		productoDao.save(producto);
		
		Producto productoActualizado= productoDao.findByNombre(nombre);
		
		assertThat(productoActualizado.getNombre()).isEqualTo(nombre);
		
	}
	
	@Test
	public void testListarProducto() {
		List<Producto> productos =  (List<Producto>) productoDao.findAll();
		
		assertThat(productos).size().isGreaterThan(0);
	}
	
	@Test
	@Rollback(false)
	public void testBorrarProducto() {
		Long id = (long) 1;
		boolean existe = productoDao.findById(id).isPresent();
		productoDao.deleteById(id);
		boolean existe2 = productoDao.findById(id).isPresent();
		
		assertTrue(existe);
		assertFalse(existe2);
	}
}
